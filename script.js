var regularExpressionInput = document.querySelector("#regExpInput");
var testSentenceInput = document.querySelector("#testSentence");
var button = document.querySelector("#testButton");
var outcome = document.querySelector("#result");
var flags = document.querySelector("#flags");

function testRegularExpression() {
    var inputRegexValue = regularExpressionInput.value;
    var regex = new RegExp(inputRegexValue, flags.value);
    var inputSentenceValue = testSentenceInput.value;
    return regex.test(inputSentenceValue);
}

button.addEventListener('click', function () {
    var result = testRegularExpression();
    outcome.innerHTML = result;
    (result === true) ? outcome.style.color = "#53DF83" : outcome.style.color = "#E3000E";
});